/// \file
/// \brief File with some useful util functions

#ifndef SECTION_EXTRACTOR_UTILS_H
#define SECTION_EXTRACTOR_UTILS_H

#include <errno.h>
#include <stdio.h>
#include <string.h>

/// Prints message in stdout (level INFO)
/// \param message - info message
void info(const char* message);

/// Prints message in stderr (level ERROR)
/// \param message - error message
void error(const char* message);

/// Prints file error is stderr (level ERROR)
/// \param filename - file name
/// \param message - error message
void error_file(const char* filename, const char* message);

/// Prints file error message associated with code placed in ERRNO
/// \param filename - file name
void error_errno(const char* filename);

#endif //SECTION_EXTRACTOR_UTILS_H
