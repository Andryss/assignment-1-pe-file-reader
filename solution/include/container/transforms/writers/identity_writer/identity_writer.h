/// \file
/// \brief Contains function which writes only section data to file

#ifndef SECTION_EXTRACTOR_IDENTITY_WRITER_H
#define SECTION_EXTRACTOR_IDENTITY_WRITER_H

#include "container/transforms/data_executor.h"

/// Writes only section data to file
/// \param to - output file
/// \param from - section info
/// \return write status
enum write_status write_identity_data(FILE* to, const struct container* from);

#endif //SECTION_EXTRACTOR_IDENTITY_WRITER_H
