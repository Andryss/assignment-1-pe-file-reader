/// \file
/// \brief File contains wrapper functions for extracting section and writing section

#ifndef SECTION_EXTRACTOR_DATA_EXECUTOR_H
#define SECTION_EXTRACTOR_DATA_EXECUTOR_H

#include "container/container_util.h"
#include "files/files_executor.h"
#include "utils/utils.h"
#include <stdio.h>

/// Enum of file read statuses
enum read_status {
    READ_OK = 0,
    READ_ERR,
    READ_INVALID_HEADER,
    READ_INVALID_SIGNATURE
};

/// Enum of file write statuses
enum write_status {
    WRITE_OK = 0,
    WRITE_ERR
};


/// Define function which reads section from file according to file format
typedef enum read_status (*read_data_func)(FILE* from, const char* section_name, struct container* to);

/// Define function which write section to file according to file format
typedef enum write_status (*write_data_func)(FILE* to, const struct container* from);


/// Enum of file convert statuses (conversion is: file -> container, container -> file)
enum convert_status {
    CONVERT_OK = 0,
    CONVERT_ERR
};


/// Wrapper function for opening file, reading section from file, closing file
/// \param filename - file name
/// \param converter - function which reads section from file
/// \param section_name - name of section to read
/// \param to - container to fill
/// \return convert status
enum convert_status read_data(const char* filename, read_data_func converter, const char* section_name, struct container* to);

/// Wrapper function for opening file, writing section to file, closing file
/// \param filename - file name
/// \param converter - function which write section to file
/// \param from - container with section content
/// \return convert status
enum convert_status write_data(const char* filename, write_data_func converter, const struct container* from);

#endif //SECTION_EXTRACTOR_DATA_EXECUTOR_H
