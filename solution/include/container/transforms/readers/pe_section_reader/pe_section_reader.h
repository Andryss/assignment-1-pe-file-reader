/// \file
/// \brief Contains function which reads section from PE file

#ifndef SECTION_EXTRACTOR_PE_SECTION_READER_H
#define SECTION_EXTRACTOR_PE_SECTION_READER_H

#include "container/container_util.h"
#include "container/transforms/data_executor.h"
#include <inttypes.h>
#include <malloc.h>
#include <stdbool.h>

/// Reads section from PE (portable executable) file
/// \param from - input file
/// \param section_name - name of section to read
/// \param to - container to fill
/// \return read status
enum read_status read_pe_section(FILE* from, const char* section_name, struct container* to);

#endif //SECTION_EXTRACTOR_PE_SECTION_READER_H
