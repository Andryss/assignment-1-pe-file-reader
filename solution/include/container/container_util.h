/// \file
/// \brief File contains some useful funcs to work with container

#ifndef SECTION_EXTRACTOR_CONTAINER_UTIL_H
#define SECTION_EXTRACTOR_CONTAINER_UTIL_H

#include <malloc.h>
#include <stdbool.h>
#include <stdio.h>

/// Container struct accumulate info about extracted section
struct container {
    /// Size of section
    size_t size;
    /// Section content
    void* data;
};

/// Allocates container with given size
/// \param c - container to fill
/// \param size - size we want to allocate
/// \return true in case of success or false otherwise
bool allocate_container(struct container* c, size_t size);

/// Frees and clean given container
/// \param c - container to clean
void free_container(struct container* c);

#endif //SECTION_EXTRACTOR_CONTAINER_UTIL_H
