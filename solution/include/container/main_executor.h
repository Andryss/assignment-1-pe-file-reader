/// \file
/// \brief File contains main function which every user will invoke

#ifndef SECTION_EXTRACTOR_MAIN_EXECUTOR_H
#define SECTION_EXTRACTOR_MAIN_EXECUTOR_H

#include "container_util.h"
#include "transforms/data_executor.h"

/// Enum of execute statuses
enum executor_status {
    EXECUTE_OK,
    EXECUTE_ERR
};

/// Main application function for open-extract-write actions
/// \param filename_src - source filename
/// \param filename_dest - destination filename
/// \param read_func - function which define how to extract a section
/// \param section_name - name of section to extract
/// \param write_func - function which define how to write extracted section
/// \return execution status
enum executor_status file_execute(
        const char* filename_src,
        const char* filename_dest,
        read_data_func read_func,
        const char* section_name,
        write_data_func write_func
);

#endif //SECTION_EXTRACTOR_MAIN_EXECUTOR_H
