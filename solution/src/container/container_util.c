/// \file
/// \brief Implementation of container_util.h

#include "container/container_util.h"

bool allocate_container(struct container* c, size_t size) {
    c->data = malloc(size);
    if (c->data == NULL) return false;
    c->size = size;
    return true;
}

void free_container(struct container* c) {
    free(c->data);
    c->data = NULL;
    c->size = 0;
}
