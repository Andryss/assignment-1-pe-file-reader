/// \file
/// \brief Implementation of pe_section_reader.h
/// \see https://learn.microsoft.com/en-us/windows/win32/debug/pe-format

#include "container/transforms/readers/pe_section_reader/pe_section_reader.h"

/// At this location is the file offset to PE signature
#define MAGIC_OFFSET 0x3C


/// Describes the content of the main header of PE file
#ifdef _MSC_VER
    #pragma pack(push, 1)
#endif
struct
#ifdef __GNUC__
    __attribute__((__packed__))
#endif
#ifdef __clang__
    __attribute__((__packed__))
#endif
main_header
{
    /// The number that identifies the type of target machine
    uint16_t machine;
    /// The number of sections. This indicates the size of the section table, which immediately follows the headers
    int16_t numberOfSections;
    /// The low 32 bits of the number of seconds since 00:00 January 1, 1970 (a C run-time time_t value), which indicates when the file was created
    int32_t timeDateStamp;
    /// The file offset of the COFF symbol table, or zero if no COFF symbol table is present
    int32_t pointerToSymbolTable;
    /// The number of entries in the symbol table
    int32_t numberOfSymbols;
    /// The size of the optional header
    int16_t sizeOfOptionalHeader;
    /// The flags that indicate the attributes of the file
    int16_t characteristics;
};
#ifdef _MSC_VER
    #pragma pack(pop)
#endif


/// Describes the content of the section header of PE file
#ifdef _MSC_VER
    #pragma pack(push, 1)
#endif
struct
#ifdef __GNUC__
    __attribute__((__packed__))
#endif
#ifdef __clang__
    __attribute__((__packed__))
#endif
section_header {
    /// An 8-byte, null-padded UTF-8 encoded string
    int64_t name;
    /// The total size of the section when loaded into memory
    int32_t virtualSize;
    /// For executable images, the address of the first byte of the section relative to the image base when the section is loaded into memory
    int32_t virtualAddress;
    /// The size of the section (for object files) or the size of the initialized data on disk (for image files)
    int32_t sizeOfRawData;
    /// The file pointer to the first page of the section within the COFF file
    int32_t pointerToRawData;
    /// The file pointer to the beginning of relocation entries for the section
    int32_t pointerToRelocations;
    /// The file pointer to the beginning of line-number entries for the section
    int32_t pointerToLinenumbers;
    /// The number of relocation entries for the section
    int16_t numberOfRelocations;
    /// The number of line-number entries for the section
    int16_t numberOfLinenumbers;
    /// The flags that describe the characteristics of the section
    int32_t characteristics;
};
#ifdef _MSC_VER
    #pragma pack(pop)
#endif


/// Accumulate all needed information about PE file
struct pe_file {
    /// \name Offsets within file
    /// @{

    /// Offset of the pointer to the main PE header (aka 'MAGIC_OFFSET')
    int32_t pointer_to_header_offset;
    /// Offset of the main PE file header
    int32_t header_offset;
    /// Offset of the section headers
    int32_t section_header_offset;

    /// @}

    /// \name PE file content
    /// @{

    /// Field to store file magic value
    int32_t magic_value;
    /// Main PE file header
    struct main_header main_header;
    /// Array of section headers
    struct section_header* section_headers;

    /// @}
};


/// Reads offset of the main PE header
/// \param from - source file
/// \param file - place to store the result
/// \return read status
static enum read_status read_header_offset(FILE* from, struct pe_file* file) {
    // Shift to header pointer
    int is_skipped = fseek(from, file->pointer_to_header_offset, SEEK_SET);
    if (is_skipped != 0) return READ_INVALID_SIGNATURE;

    // Try to read header pointer
    size_t read_count = fread(&file->header_offset, sizeof(file->header_offset), 1, from);
    if (read_count != 1) return READ_INVALID_SIGNATURE;

    return READ_OK;
}

/// Reads main PE file header
/// \param from - source file
/// \param file - place to store the result
/// \return read status
static enum read_status read_main_header(FILE* from, struct pe_file* file) {
    // Shift to main header
    int is_skipped = fseek(from, file->header_offset, SEEK_SET);
    if (is_skipped != 0) return READ_INVALID_SIGNATURE;

    // Try to read magic
    size_t read_count = fread(&file->magic_value, sizeof(file->magic_value), 1, from);
    if (read_count != 1) return READ_INVALID_SIGNATURE;

    // Try to read main header
    read_count = fread(&file->main_header, sizeof(struct main_header), 1, from);
    if (read_count != 1) return READ_INVALID_SIGNATURE;

    if (file->main_header.numberOfSections < 1) return READ_INVALID_HEADER;

    return READ_OK;
}

/// Help function which allocate memory for section headers
/// \param file - place to store the result
/// \return true in case of success or false otherwise
static bool allocate_section_headers(struct pe_file* file) {
    file->section_headers = malloc(file->main_header.numberOfSections * sizeof(struct section_header));
    if (file->section_headers == NULL) return false;
    else return true;
}

/// Free memory allocated for section headers
/// \param file - place to free and clean the section headers
static void free_section_headers(struct pe_file* file) {
    free(file->section_headers);
    file->section_headers = NULL;
}

/// Reads section headers according to header info
/// \param from - source file
/// \param file - place to store the result
/// \return read status
static enum read_status read_section_headers(FILE* from, struct pe_file* file) {
    // Calculate the offset
    file->section_header_offset = file->header_offset + (int32_t) sizeof(file->magic_value) +
            (int32_t) sizeof(file->main_header) + file->main_header.sizeOfOptionalHeader;

    // Shift to section headers
    int is_skipped = fseek(from, file->section_header_offset, SEEK_SET);
    if (is_skipped != 0) return READ_INVALID_SIGNATURE;

    // Try to read section headers
    size_t read_count = fread(file->section_headers, sizeof(struct section_header), file->main_header.numberOfSections, from);
    if (read_count != file->main_header.numberOfSections) return READ_INVALID_HEADER;

    return READ_OK;
}

/// Reads sections content
/// \param from - source file
/// \param section - section header
/// \param to - container to fill
/// \return read status
static enum read_status read_section_data(FILE* from, struct section_header* section, struct container* to) {
    // Shift to section data
    int is_skipped = fseek(from, section->pointerToRawData, SEEK_SET);
    if (is_skipped != 0) return READ_INVALID_SIGNATURE;

    // Try to allocate container
    if (!allocate_container(to, section->sizeOfRawData)) return READ_ERR;

    // Try to read data
    size_t read_count = fread(to->data, to->size, 1, from);
    if (read_count != 1) {
        free_container(to);
        return READ_INVALID_SIGNATURE;
    }

    return READ_OK;
}

/// Tries to find and extract section with given name
/// \param from - source file
/// \param file - PE file info
/// \param section_name - name of section to find
/// \param to - container to fill
/// \return true in case of success or false otherwise
static bool extract_section(FILE* from, struct pe_file* file, const char* section_name, struct container* to) {
    // Iterate through sections
    for (size_t i = 0; i < file->main_header.numberOfSections; ++i) {
        // Choose the section
        struct section_header *section_header = file->section_headers + i;

        // Compare the name
        if (strncmp((char*) &(section_header->name), section_name, sizeof(section_header->name)) != 0) continue;

        // Try to read section data
        enum read_status read_section_data_status = read_section_data(from, section_header, to);
        if (read_section_data_status != READ_OK) return false;

        return true;
    }
    return false;
}

/// Wrapper function for reading main PE header
/// \param from - source file
/// \param file - PE file info
/// \return read status
static enum read_status read_pe_file_header(FILE* from, struct pe_file* file) {
    // Set magic offset
    file->pointer_to_header_offset = MAGIC_OFFSET;

    // Read header offset
    enum read_status read_header_offset_status = read_header_offset(from, file);
    if (read_header_offset_status != READ_OK) return read_header_offset_status;

    // Read magic + header
    enum read_status read_main_header_status = read_main_header(from, file);
    if (read_main_header_status != READ_OK) return read_main_header_status;

    return READ_OK;
}

/// Wrapper function for reading and extracting section
/// \param from - source file
/// \param file - PE file info
/// \param section_name - name of section
/// \param to - container
/// \return read status
static enum read_status read_and_extract_pe_file_section(FILE* from, struct pe_file* file, const char* section_name, struct container* to) {
    // Allocate section headers
    if (!allocate_section_headers(file)) return READ_ERR;

    // Read section headers
    enum read_status read_section_headers_status = read_section_headers(from, file);
    if (read_section_headers_status != READ_OK) {
        free_section_headers(file);
        return read_section_headers_status;
    }

    // Extract section
    bool is_found = extract_section(from, file, section_name, to);
    free_section_headers(file);
    if (!is_found) return READ_ERR;

    return READ_OK;
}


enum read_status read_pe_section(FILE* from, const char* section_name, struct container* to) {
    // Init PE file info
    struct pe_file file = {0};

    // Try to read header
    enum read_status read_pe_file_header_status = read_pe_file_header(from, &file);
    if (read_pe_file_header_status != READ_OK) return read_pe_file_header_status;

    // Try to read and extract section
    enum read_status read_pe_file_section_status = read_and_extract_pe_file_section(from, &file, section_name, to);
    if (read_pe_file_section_status != READ_OK) return read_pe_file_section_status;

    return READ_OK;
}
