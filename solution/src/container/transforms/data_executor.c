/// \file
/// \brief Implementation of data_executor.h

#include "container/transforms/data_executor.h"

/// Array with error messages
const char* const open_errors[] = {
        [OPEN_ERR] = "Some error occurred while opening file\n"
};

/// Array with error messages
const char* const read_errors[] = {
        [READ_ERR] = "Some error occurred while reading from file\n",
        [READ_INVALID_HEADER] = "Invalid header found\n",
        [READ_INVALID_SIGNATURE] = "Invalid file signature\n"
};

/// Array with error messages
const char* const write_errors[] = {
        [WRITE_ERR] = "Some error occurred while writing into file\n"
};

/// Array with error messages
const char* const close_errors[] = {
        [CLOSE_ERR] = "Some error occurred while closing file\n"
};

enum convert_status read_data(const char* filename, read_data_func converter, const char* section_name, struct container* to) {
    FILE* file = NULL;
    // Try to open file
    enum open_status open_file_status = file_open(&file, filename, "rb");
    if (open_file_status != OPEN_OK) {
        error_errno(filename);
        error_file(filename, open_errors[open_file_status]);
        return CONVERT_ERR;
    }

    // Try to read section with given converter
    enum read_status read_data_status = converter(file, section_name, to);

    // Try to close file (doesn't depend on conversion result)
    enum close_status close_file_status = file_close(file);
    if (close_file_status != CLOSE_OK) {
        error_errno(filename);
        error_file(filename, close_errors[close_file_status]);
        return CONVERT_ERR;
    }

    // If conversion failed
    if (read_data_status != READ_OK) {
        error_file(filename, read_errors[read_data_status]);
        return CONVERT_ERR;
    }

    return CONVERT_OK;
}

enum convert_status write_data(const char* filename, write_data_func converter, const struct container* from) {
    FILE* file = NULL;
    // Try to open file
    enum open_status open_file_status = file_open(&file, filename, "wb");
    if (open_file_status != OPEN_OK) {
        error_errno(filename);
        error_file(filename, open_errors[open_file_status]);
        return CONVERT_ERR;
    }

    // Try to write section with given converter
    enum write_status write_data_status = converter(file, from);

    // Try to close file (doesn't depend on conversion result)
    enum close_status close_file_status = file_close(file);
    if (close_file_status != CLOSE_OK) {
        error_errno(filename);
        error_file(filename, close_errors[close_file_status]);
        return CONVERT_ERR;
    }

    // If conversion failed
    if (write_data_status != WRITE_OK) {
        error_file(filename, write_errors[write_data_status]);
        return CONVERT_ERR;
    }

    return CONVERT_OK;
}
