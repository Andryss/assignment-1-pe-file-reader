/// \file
/// \brief Implementation of identity_writer.h

#include "container/transforms/writers/identity_writer/identity_writer.h"

enum write_status write_identity_data(FILE* to, const struct container* from) {
    // Write container data to file
    size_t write_count = fwrite(from->data, from->size, 1, to);
    if (write_count != 1) return WRITE_ERR;

    return WRITE_OK;
}
