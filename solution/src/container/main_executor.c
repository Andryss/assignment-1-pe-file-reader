/// \file
/// \brief Implementation of main_executor.h

#include "container/main_executor.h"

/// Array with error messages
char* convert_errors[] = {
        [CONVERT_ERR] = "Some error occurred while working with file\n"
};

enum executor_status file_execute(
        const char* filename_src,
        const char* filename_dest,
        read_data_func read_func,
        const char* section_name,
        write_data_func write_func
) {

    // Init container
    struct container container = {0};

    // Try to read section into container
    enum convert_status read_image_status = read_data(filename_src, read_func, section_name, &container);
    if (read_image_status != CONVERT_OK) {
        error_file(filename_src, convert_errors[read_image_status]);
        return EXECUTE_ERR;
    }

    // Try to write section into file
    enum convert_status write_image_status = write_data(filename_dest, write_func, &container);
    free_container(&container);
    if (write_image_status != CONVERT_OK) {
        error_file(filename_dest, convert_errors[write_image_status]);
        return EXECUTE_ERR;
    }

    return EXECUTE_OK;
}
