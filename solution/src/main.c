/// \file
/// \brief Main application file

#include "utils/utils.h"
#include "container/main_executor.h"
#include "container/transforms/readers/pe_section_reader/pe_section_reader.h"
#include "container/transforms/writers/identity_writer/identity_writer.h"
#include <sysexits.h>

/// Array with error messages
static const char* const execute_messages[] = {
        [EXECUTE_OK] = "Section extractor successfully finished\n",
        [EXECUTE_ERR] = "Some error occurred during extractor working\n"
};

/// Application entry point
/// \param argc - Number of command line arguments
/// \param argv - Command line arguments
/// \return 0 in case of success or error code
int main(int argc, char** argv) {

    // Arg checking
    if (argc != 4) {
        error("Usage: section-extractor <source file> <section name> <output file>\n");
        return EX_USAGE;
    }

    // Invoke main function
    enum executor_status section_extract_status = file_execute
            (
                    argv[1],
                    argv[3],
                    read_pe_section,
                    argv[2],
                    write_identity_data
            );

    // Print computation result
    if (section_extract_status != EXECUTE_OK) {
        error(execute_messages[section_extract_status]);
        return EX__BASE;
    } else {
        info(execute_messages[EXECUTE_OK]);
        return EX_OK;
    }
}
